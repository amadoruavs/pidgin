# Branches in the AmadorUAVs Repos #
NOTE: This documentation page assumes you have a basic
understanding of Git commands. If not, please read [USING_GIT.md](USING_GIT.md).

All repositories under the AmadorUAVs Software Team 
follow a modified version of [Github Flow](https://guides.github.com/introduction/flow/) 
with Continuous Integration. Here's an outline of how 
to use the branching strategy:

## Creating a new branch ##
1. If there is no issue for the bugfix/feature you
want to implement, log one in the Issue Tracker.
2. Checkout the `develop` branch. This sets `develop` as
your base branch. DO NOT BASE YOUR BRANCH ON MASTER. IT WILL
BE OUT OF DATE.
3. Create a new issue branch named `iss[number]/[short-description]`
with `git checkout -b [branch]`.

## Developing on an issue branch ##
1. Write your code.
2. Commit your code **at least daily**. This is very
important to the concept of CI/CD; it makes sure
that everyone else is up to date and avoids
merge issues.
3. `git fetch && git merge origin/develop`
**before** pushing. Fix any merge issues before you push.
4. `git push`. The CI scripts will check your code and
merge it into develop if it passes.

## Closing the issue branch ##
1. Submit a merge request for your branch into `master`.
Your code is already merged into develop, but submitting
the merge request is a formal way to declare that you are
finished with the branch - and since the branch will no longer
be modified, it is safe to merge it into `master`.
2. Check the "Close Branch" option on the merge request.
