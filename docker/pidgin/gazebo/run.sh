#!/bin/sh
xhost + # allow connections to X server
docker run --privileged -e "DISPLAY=unix:0.0" -v="/tmp/.X11-unix:/tmp/.X11-unix:rw" -p 14580:14540 -p 14570:14550 -t registry.gitlab.com/avhs-suas/pidgin/gazebo
