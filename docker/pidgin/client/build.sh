# Builds, tags and (optionally) pushes common images

# Automatically exit on any command failure
set -e

# idiomatic parameter and option handling in sh
# Copied from SuperUser/StackExchange
TOBUILD="DEFAULT"
PUSH="FALSE"

# Define the help menu manually because apparently you need to in bash
help() {
    echo "Usage: ./build.sh [--push] [--help]"
    echo
    echo "  --push      Push images to registry once built"
    echo "  --help      Print this help"
}

while test $# -gt 0
do
    case "$1" in
        --push) echo "Pushing Docker images to registry when done."
               PUSH="TRUE"
            ;;
        --help) help; exit 0
            ;;
        *) echo "WARN: Unrecognized argument $1, ignoring"
            ;;
    esac
    shift
done

# Build
sudo docker build --no-cache -t registry.gitlab.com/amadoruavs/pidgin/suasclient .
if [ "$PUSH" == "TRUE" ]; then
    # Push
    sudo docker push registry.gitlab.com/amadoruavs/pidgin/suasclient
fi

