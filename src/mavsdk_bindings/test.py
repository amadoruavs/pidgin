import mavsdk_bindings
import time

drone = mavsdk_bindings.Drone("udp://gazebo:14550")
# Return first
drone.return_to_launch()
time.sleep(5)

drone.new_mission()
waypoint1 = mavsdk_bindings.Waypoint()
waypoint1.set_latitude(38.1449665)
waypoint1.set_longitude(-76.4342404)
waypoint1.set_height(10)

waypoint2 = mavsdk_bindings.Waypoint()
waypoint2.set_latitude(38.14605)
waypoint2.set_longitude(-76.4342404)
waypoint2.set_height(10)

drone.add_waypoint(waypoint1)
drone.add_waypoint(waypoint2)
drone.print_mission()

drone.takeoff(10)
drone.start_mission()
for i in range(20):
    pos = drone.get_position()
    print(pos.latitude(), pos.longitude(), pos.height())
    time.sleep(1)
print("landing!")
drone.return_to_launch()
