/* This file contains the interface file for building
   the MAVSDK C++ Python-accessible SWIG API. */

/* Set the name of the module */
%module mavsdk_bindings

/* Include typemaps for primitives and std::string so
   Python datatypes can be converted to C++ */
%include <typemaps.i>
%include <std_string.i>

/* Include header files (drone.h + the mavsdk library) */
%{
#include <mavsdk/mavsdk.h>
#include "drone.h"
#include "data.h"
%}

/* Grab the header to autogenerate the SWIG interface */
%include "drone.h"
%include "data.h"
