from auvsi_suas.client import types
from auvsi_suas.client import client
from auvsi_suas.client.types import StationaryObstacle
from math import pow, asin

client = client.Client(url='http://127.0.0.1:8000',
                        username='suasdev',
                        password='AVuav2018')

def is_similar(num1, num2, thresh=5):
    if abs(num1-num2) <= thresh:
        return True
    return False

missions = client.get_missions()
stationary_obstacles = client.get_obstacles()

UAVData = client.post_telemetry("post telemetry data here")
bearing = UAVData.uas_heading

for obstacle in stationary_obstacles:
    if is_similar(obstacle.latitude, UAVData.latitude):
        if UAVData.altitude_msl < obstacle.cylinder_height:
            hyp = pow(missions[0].mission_waypoints.latitude, 2) - pow(UAVData.latitude, 2)
            bearing = asin(hyp)



